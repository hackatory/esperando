Rails.application.routes.draw do
  get 'calendar/index'

  root to: 'borns#index'

  resources :borns, only: [:show, :new, :create, :index] do
    resources :bets, only: [:new, :create, :edit, :update]
  end

  get '/:name', to: 'borns#show'
end
