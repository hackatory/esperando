class Born < ApplicationRecord
  has_many :bets

  validates :name, presence: true
  validates :birthday, presence: true
end
